# Ready4S Recruitment Automation

## 1. Prerequisites

0. Test is executed from Firefox browser so please be sure it's installed before
1. Please download and install Git [from here](https://github.com/git-for-windows/git/releases/download/v2.30.2.windows.1/Git-2.30.2-64-bit.exe)
2. Please download and install Python in version 3.8.6 [from here](https://www.python.org/ftp/python/3.8.6/python-3.8.6-amd64.exe)
3. During installation please **Add to PATH** and **Disable PATH limit** when requested
4. Please execute **pre-req.bat** to install all dependencies for the project
5. Run **run.bat** to run program

## 2. Outcome
Log from behave can be found in main folder under **behave.log**