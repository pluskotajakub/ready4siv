Feature: I want to make an order

#  Scenario: Add products to the cart by thumbnail
#    Given I am logged in
#    When I add product by the thumbnail <- I didn't figure the way of testing this point automaticly so it is skipped
#    Then My product is in the cart

  Scenario: Add products to the cart by PD page
    Given I am logged in
    When I go to the product detail page
    And Add product to cart
    Then My product is in the cart

  Scenario: Go to checkout
    Given I have products in my cart
    When I go to the checkout
    Then I see 4 progress steps
    And First one is in pink
    # the color should be rgb(255,192,203) | #FFC0CB
    And Product, Available, Unit Price, Quantity, Total and Remove button are visible
    And Total products price is correct
    And Delivery & Invoice address is visible