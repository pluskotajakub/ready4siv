Feature: Login to the account

  Scenario Outline: Sign-in to user account
    Given User is registered
    When User is trying to login with correct "<email>" and "<password>"
    Then User is successfully logged in

    Examples: Valid login information
      | email               | password |
      | johndoe@example.com | johndoe  |