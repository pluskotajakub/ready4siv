from selenium import webdriver
from behave.model import Scenario

def before_all(context):
    context.config.setup_logging()
    userdata = context.config.userdata
    continue_after_failed = userdata.getbool("runner.continue_after_failed_step", True)
    Scenario.continue_after_failed_step = continue_after_failed

def before_feature(context, feature):
    if feature.name == 'Login to the account':
        context.driver = webdriver.Firefox(executable_path="geckodriver.exe")
        context.driver.get('http://automationpractice.com')
    if feature.name == 'I want to make an order':
        context.driver = webdriver.Firefox(executable_path="geckodriver.exe")
        context.action = webdriver.ActionChains(context.driver)
        context.driver.get('http://automationpractice.com')
        context.driver.find_element_by_css_selector('.login').click()
        context.driver.find_element_by_css_selector('#email').send_keys('johndoe@example.com')
        context.driver.find_element_by_css_selector('#passwd').send_keys('johndoe')
        context.driver.find_element_by_css_selector('#SubmitLogin').click()

def after_feature(context, feature):
    if feature.name == 'I want to make an order' or feature.name == 'Login to the account':
        context.driver.quit()