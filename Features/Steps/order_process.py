from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from behave import given, when, then, step
from time import sleep


@given(u'I am logged in')
def step_impl(context):
    try:
        context.driver.find_element_by_css_selector('.logout') #checks if .logout element is found - if not then we are logged in
    except NoSuchElementException:
        assert False

# @when(u'I add product by the thumbnail') #unused, not working step from #order_process.feature
# def step_impl(context):
#     context.driver.get('http://automationpractice.com/index.php')
#     action = webdriver.ActionChains(context.driver)
#     action.move_to_element(context.driver.find_element_by_css_selector('#homefeatured > li:nth-child(1) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1)'))
#     context.driver.find_element_by_css_selector('#homefeatured > li:nth-child(1) > div:nth-child(1) > div:nth-child(2) > div:nth-child(4) > a:nth-child(1)').click()

@when(u'I go to the product detail page')
def step_impl(context):
    context.driver.get('http://automationpractice.com/index.php?id_product=1&controller=product')

@step(u'Add product to cart')
def step_impl(context):
    context.driver.find_element_by_css_selector('button.exclusive').click()

@then(u'My product is in the cart')
def step_impl(context):
    sleep(3) #sleep is for waiting for execution of script on the site - without it buy procedure will fail with script error on server-side
    pd_name = context.driver.find_element_by_css_selector('#layer_cart_product_title').text #name of the product on buy popup confirmation
    print('Name on add to cart confirmation: ' + pd_name)
    context.driver.get('http://automationpractice.com/index.php?controller=order')
    cart_name = context.driver.find_element_by_css_selector('td.cart_description > p:nth-child(1) > a:nth-child(1)').text #name of the product in the cart
    print('Name on cart: ' + cart_name)
    assert pd_name == cart_name

@given(u'I have products in my cart')
def step_impl(context):
    try:
        context.driver.find_element_by_class_name('ajax_cart_quantity unvisible') #if class unvisible is not found then we have products in the cart
    except NoSuchElementException:
        assert True

@when(u'I go to the checkout')
def step_impl(context):
    context.driver.get('http://automationpractice.com/index.php?controller=order')

@then(u'I see 4 progress steps')
def step_impl(context):
    steps = context.driver.find_element_by_css_selector('#order_step')
    steps_count = len(steps.find_elements_by_tag_name('li')) #takes number of elements in progress steps
    if steps_count == 4:
        assert True
    else:
        assert False

@step(u'First one is in pink')
def step_impl(context):
    first_step = context.driver.find_element_by_css_selector('.step_current > span:nth-child(1)')
    expected_color = 'rgb(255,192,203)' #pink
    real_color = first_step.value_of_css_property('border-bottom-color')
    if expected_color == real_color:
        assert True
    else:
        print('Expected color = ' + expected_color + '\n' + 'Real color = ' + real_color)
        assert False

@step(u'Product, Available, Unit Price, Quantity, Total and Remove button are visible')
def step_impl(context): #all elements below checks if content is visible on the page - it should be done differently but for the sake of assignment it is done like that :)
    context.driver.find_element_by_css_selector('td.cart_description').is_displayed()
    context.driver.find_element_by_css_selector('td.cart_avail').is_displayed()
    context.driver.find_element_by_css_selector('td.cart_unit').is_displayed()
    context.driver.find_element_by_css_selector('td.cart_quantity').is_displayed()
    context.driver.find_element_by_css_selector('td.cart_total').is_displayed()
    context.driver.find_element_by_css_selector('td.cart_delete').is_displayed()

@step(u'Total products price is correct')
def step_impl(context):
    unit_price = context.driver.find_element_by_css_selector('#product_price_1_1_461613 > span:nth-child(1)').text[1:]
    quantity = context.driver.find_element_by_css_selector('.cart_quantity_input').get_attribute('value')
    expected_price = float(unit_price) * int(quantity)
    real_price = context.driver.find_element_by_css_selector('#total_product').text[1:]
    if float(real_price) == float(expected_price):
        assert True
    else:
        assert False

@step(u'Delivery & Invoice address is visible')
def step_impl(context):
    context.driver.find_element_by_css_selector('ul.first_item').is_displayed()
    context.driver.find_element_by_css_selector('ul.last_item').is_displayed()


