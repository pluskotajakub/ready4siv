from behave import given, when, then

from selenium import webdriver

@given(u'User is registered')
def step_impl(context):
    assert True

@when(u'User is trying to login with correct "{email}" and "{password}"')
def step_impl(context, email, password):
    context.driver.find_element_by_css_selector('.login').click()
    context.driver.find_element_by_css_selector('#email').send_keys(email)
    context.driver.find_element_by_css_selector('#passwd').send_keys(password)
    context.driver.find_element_by_css_selector('#SubmitLogin').click()

@then(u'User is successfully logged in')
def step_impl(context):
    assert context.driver.find_element_by_css_selector('.info-account').text == 'Welcome to your account. Here you can manage all of your personal information and orders.'